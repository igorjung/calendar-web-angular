import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dayOfMonth'
})
export class DayOfMonthPipe implements PipeTransform {
  transform(value: string): string {
    return moment(value).format('DD');
  }
}
