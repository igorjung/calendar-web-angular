export interface IEvent {
  id: number,
  title: string,
  description?: string,
  startAt: string,
  endAt: string,
  tz: string,
  guests: string[],
  isGuest: boolean,
}

export interface IEventCard {
  id: number,
  title: string,
  description?: string,
  startAt: string,
  endAt: string,
  tz: string,
  x: number,
  y: number,
  h: number
  w: string,
  guests: string[],
  isGuest: boolean,
}

export interface ICreateEvent {
  title: string,
  description?: string,
  startAt: string,
  endAt: string,
  tz: string,
}

export interface ResponseGetEvents {
  getEventsFromUser: IEvent[]
}

export interface ResponseGetEvent {
  getEvent: IEvent
}

export interface ResponseCreateEvent {
  createEvent: IEvent
}
