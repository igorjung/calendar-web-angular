import { IEventCard } from './event.model';

export interface ISchedule {
  days: string[],
  tz: string,
  hours: string[],
  events: IEventCard[],
}
