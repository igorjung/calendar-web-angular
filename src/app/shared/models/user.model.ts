export interface IUser {
  id: number,
  email: string,
  token: string,
}

export interface ILogin {
  email: string,
  password: string,
}

export interface ResponseLogin {
  login: IUser
}

export interface ResponseCreateUser {
  createUser: IUser
}
