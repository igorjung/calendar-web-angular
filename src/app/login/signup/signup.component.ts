import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { LoginService } from '../login.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
})
export class SignupComponent {
  signupForm = this.formBuilder.group({
    email: '',
    password: '',
  });
  loading: boolean = false;
  error: any;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
  ) { }

  onSubmit(): void {
    let value = this.signupForm.value;
    this.loginService.signup(value);
  }
}
