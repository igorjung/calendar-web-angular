import { Injectable, EventEmitter, OnDestroy } from '@angular/core';
import {Apollo, gql} from 'apollo-angular';
import * as moment from 'moment';
import { Subscription } from 'rxjs';

import {
  IEvent,
  ICreateEvent,
  ResponseGetEvents,
  ResponseGetEvent,
  ResponseCreateEvent
} from '../shared/models/event.model';

@Injectable({
  providedIn: 'root'
})
export class CalendarService implements OnDestroy {
  start: string;
  date: string;
  type: string;
  events: IEvent[];

  newType = new EventEmitter<string>();
  newStartDate = new EventEmitter<string>();
  newDate = new EventEmitter<string>();
  newEvents = new EventEmitter<IEvent[]>();
  newEvent = new EventEmitter<IEvent>();
  subscriptions: Subscription[] = [];

  constructor(private apollo: Apollo) {
    this.start = localStorage.getItem('start') ||
      moment(new Date()).format('ddd') === 'Sun' ?
      moment(new Date()).format('yyyy-MM-DD HH:mm:ss') :
      this.getLastSunday(moment(new Date()).format('yyyy-MM-DD HH:mm:ss'));
    this.date = localStorage.getItem('end') || moment(new Date()).format('yyyy-MM-DD HH:mm:ss');
    this.type = localStorage.getItem('type') || 'Week';
    this.events = [];
  }

  /* Events CRUD */

  eventQuery() {
    const user = JSON.parse(localStorage.getItem('user') || '');
    const type = localStorage.getItem('type');

    const begin = moment(
      localStorage.getItem('start')
    ).startOf('day')
    .format('yyyy-MM-DD HH:mm:ss');
    let end = moment(
      localStorage.getItem('start')
    ).endOf('day')
    .format('yyyy-MM-DD HH:mm:ss');

    if(type === 'Week') {
      end = moment(
        localStorage.getItem('start')
      ).add(7, 'days')
      .endOf('day')
      .format('yyyy-MM-DD HH:mm:ss');
    }

    return {
      query: gql`
        query GetEventsFromUser(
          $userId: Float!,
          $begin: String!,
          $end: String!
        ) {
          getEventsFromUser(
            userId: $userId,
            begin: $begin,
            end: $end
          ) {
            id
            title
            description
            startAt
            endAt
            tz
            guests
            isGuest
          }
        }
      `,
      variables: {
        userId: user.id,
        begin,
        end
      },
    }
  }

  listEvents() {
    const querySubscription = this.apollo.watchQuery<ResponseGetEvents>(
      this.eventQuery(),
    ).valueChanges.subscribe(({ data }) => {
      this.events = data.getEventsFromUser;
      this.newEvents.emit(this.events);
    });

    this.subscriptions = [...this.subscriptions, querySubscription];
  }

  getEvent(id: number) {
    const user = JSON.parse(localStorage.getItem('user') || '');
    const querySubscription = this.apollo.watchQuery<ResponseGetEvent>({
      query: gql`
        query GetEvent($id: Float!, $userId: Float!) {
          getEvent(id: $id, userId: $userId) {
            id
            title
            description
            startAt
            endAt
            tz
            guests
            isGuest
          }
        }
      `,
      variables: {
        id: id,
        userId: user.id,
      },
    }).valueChanges.subscribe(({ data }) => {
      this.newEvent.emit(data.getEvent);
    });

    this.subscriptions = [...this.subscriptions, querySubscription];
  }

  createEvent(event: ICreateEvent, guests: string) {
    const user = JSON.parse(localStorage.getItem('user') || '');
    const querySubscription = this.apollo.mutate<ResponseCreateEvent>({
      mutation: gql`
        mutation CreateEvent($data: EventInput!, $guests: String!) {
          createEvent(data: $data, guests: $guests) {
            id
            title
            description
            startAt
            endAt
            tz
          }
        }
      `,
      variables: {
        data: {
          ...event,
          userId: user.id,
        },
        guests: guests,
      },
      refetchQueries: [this.eventQuery()]
    }).subscribe();

    this.subscriptions = [...this.subscriptions, querySubscription];
  }

  editEvent(event: ICreateEvent, id: number, guests: string) {
    const user = JSON.parse(localStorage.getItem('user') || '');
    const querySubscription = this.apollo.mutate<ResponseCreateEvent>({
      mutation: gql`
        mutation UpdateEvent(
          $id: Float!,
          $data: EventInput!,
          $guests: String!,
        ) {
          updateEvent(id: $id, data: $data, guests: $guests) {
            id
            title
            description
            startAt
            endAt
            tz
          }
        }
      `,
      variables: {
        id: id,
        data: {
          ...event,
          userId: user.id,
        },
        guests
      },
      refetchQueries: [this.eventQuery()]
    }).subscribe();

    this.subscriptions = [...this.subscriptions, querySubscription];
  }

  deleteEvent(id: number) {
    const querySubscription = this.apollo.mutate({
      mutation: gql`
        mutation DeleteEvent($id: Float!) {
          deleteEvent(id: $id){
            id
          }
        }
      `,
      variables: { id: id },
      refetchQueries: [this.eventQuery()]
    }).subscribe();

    this.subscriptions = [...this.subscriptions, querySubscription];
  }

  /* Week's days handle */

  getLastSunday(startDay:string, ):string {
    let newDate:string = '';
    let isSunday: boolean = false;
    let days: number = 1;

    while(!isSunday) {
      newDate = moment(startDay).subtract(days, 'days').format('ddd');
      if(newDate === 'Sun') {
        isSunday = true;
        newDate = moment(startDay).subtract(days, 'days').format('yyyy-MM-DD HH:mm:ss');
      } else {
        days++;
      }
    }

    return newDate;
  }

  getNextSunday(startDay:string, ):string {
    let newDate:string = '';
    let isSunday: boolean = false;
    let days: number = 1;

    while(!isSunday) {
      newDate = moment(startDay).add(days, 'days').format('ddd');
      if(newDate === 'Sun') {
        isSunday = true;
        newDate = moment(startDay).add(days, 'days').format('yyyy-MM-DD HH:mm:ss');
      } else {
        days++;
      }
    }

    return newDate;
  }

  /* Date and Type values */

  getDefaultValues() {
    this.newType.emit(this.type);
    this.newStartDate.emit(this.start);
    this.newDate.emit(this.date);

    this.setNewStart(this.start);
  }

  setNewType(type: string) {
    this.type = type;
    localStorage.setItem('type', this.type);
    this.newType.emit(this.type);
    this.setNewStart(this.date);
  }

  setNewStart(newDate: string, value?: string) {
    this.start = newDate;

    if(value) {
      if(this.type === 'Day') {
        if(value === 'next') {
          newDate = moment(this.start).add(1, 'days').format('yyyy-MM-DD HH:mm:ss');
          this.date = newDate;
        } else if (value === 'previous') {
          newDate = moment(this.start).subtract(1, 'days').format('yyyy-MM-DD HH:mm:ss');
          this.date = newDate;
        }
      }

      if(this.type === 'Week') {
        if(value === 'next') {
          newDate = this.getNextSunday(this.start);
          this.date = moment(this.date).add(7, 'days').format('yyyy-MM-DD HH:mm:ss');
        } else if (value === 'previous') {
          newDate = this.getLastSunday(this.start);
          this.date = moment(this.date).subtract(7, 'days').format('yyyy-MM-DD HH:mm:ss');
        }
      }
    } else {
      this.date = newDate;
      if(this.type === 'Week' && moment(this.start).format('ddd') !== 'Sun') {
        newDate = this.getLastSunday(this.start);
      }
    }

    this.start = newDate;
    localStorage.setItem('start', this.start);
    localStorage.setItem('date', this.date);
    this.newStartDate.emit(this.start);
    this.newDate.emit(this.date);
    this.listEvents();

  }

  ngOnDestroy(): void {
    for (let sub of this.subscriptions) {
      if (sub && sub.unsubscribe) {
        sub.unsubscribe();
      }
    }
  }
}
