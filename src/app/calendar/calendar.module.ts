import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DetailsComponent } from './details/details.component';
import { NewComponent } from './new/new.component';
import { EditComponent } from './edit/edit.component';

import { HeaderComponent } from './components/header/header.component';
import { SchedulerComponent } from './components/scheduler/scheduler.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ModalNewComponent } from './components/modal-new/modal-new.component';
import { EventComponent } from './components/event/event.component';

import { CalendarRoutingModule } from './calendar-routing.module';

import { MonthOfYearPipe } from '../shared/pipes/month-of-year.pipe';
import { DayOfWeekPipe } from '../shared/pipes/day-of-week.pipe';
import { DayOfMonthPipe } from '../shared/pipes/day-of-month.pipe';
import { HourPipe } from '../shared/pipes/hour.pipe';
import { HourOfDayPipe } from '../shared/pipes/hour-of-day.pipe';
import { ModalEditComponent } from './components/modal-edit/modal-edit.component';

@NgModule({
  declarations: [
    DetailsComponent,
    NewComponent,
    EditComponent,
    HeaderComponent,
    SchedulerComponent,
    SidebarComponent,
    MonthOfYearPipe,
    DayOfWeekPipe,
    DayOfMonthPipe,
    HourPipe,
    HourOfDayPipe,
    ModalNewComponent,
    EventComponent,
    ModalEditComponent,
  ],
  imports: [
    CommonModule,
    CalendarRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class CalendarModule { }
