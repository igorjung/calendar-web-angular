import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

import { IEvent } from '../../shared/models/event.model';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
})
export class DetailsComponent implements OnInit {
  start: string =  moment(new Date()).format('yyyy-MM-DD');
  model: {year: number, month: number, day: number} = {
    year: parseInt(moment(new Date()).format('YYYY'), 10),
    month: parseInt(moment(new Date()).format('MM'), 10),
    day: parseInt(moment(new Date()).format('DD'), 10),
  };
  events: IEvent[] = [];
  type: string = 'Week';
  content: any = null;

  constructor(private calendarService: CalendarService) {}

  ngOnInit() {
    this.calendarService.newType.subscribe((type) => {
      this.type = type;
    });

    this.calendarService.newStartDate.subscribe((start) => {
      this.start = start;
    });

    this.calendarService.newDate.subscribe((date) => {
      this.model = {
        year: parseInt(moment(date).format('YYYY'), 10),
        month: parseInt(moment(date).format('MM'), 10),
        day: parseInt(moment(date).format('DD'), 10),
      }
    });

    this.calendarService.newEvents.subscribe((events) => {
      this.events = events;
    });

    this.calendarService.getDefaultValues();
  }
}
