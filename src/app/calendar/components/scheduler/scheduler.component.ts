import { Component, Input, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import * as momentTz from 'moment-timezone';

import { ISchedule } from '../../../shared/models/schedule.model';
import { IEvent, IEventCard } from '../../../shared/models/event.model';
import { defaultDays, defaultHours } from '../../../shared/utils/default-values';

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
})
export class SchedulerComponent implements OnChanges {

  @Input() start: string = moment(new Date()).format('yyyy-MM-DD HH:mm:ss');
  @Input() type:string = 'Week';
  @Input() events: IEvent[] = [];
  schedule: ISchedule = {
    days: defaultDays,
    tz: 'GMT-03',
    hours: defaultHours,
    events: [],
  };
  currentDay: string = moment(new Date()).format('yyyy-MM-DD');

  constructor(private router:Router) {}

  generateWeekSchedule() {
    let days: string[] = [];
    let eventCards: IEventCard[] = [];

    for (let i = 0; i < 7; i++) {
      const newDay = moment(this.start)
        .add(i, 'days')
        .set({
          hour: 0,
          minute: 0,
          second: 0,
        })
        .format('yyyy-MM-DD');
      days.push(newDay);
    }

    this.schedule.days = days;
    days.forEach((day, index) => {
      const start = moment(day).startOf('day');
      const daysEvents = this.events.filter(event =>
        moment(start).isSame(event.startAt, 'day') ||
        moment(day).isBetween(event.startAt, event.endAt)
      );

      daysEvents.forEach(event => {
        let h =
          parseInt(moment(event.endAt).format('HH'),10) -
          parseInt(moment(event.startAt).format('HH'), 10);
        h = h > 24 ? 24 : h;

        eventCards.push({
          ...event,
          x: index + 1,
          y: parseInt(moment(event.startAt).format('HH'), 10) + 1,
          h,
          w: '112px',
        });
      });
    });

    this.schedule.events = eventCards;
  }

  generateDaySchedule() {
    const start: string = moment(this.start).startOf('day').format('yyyy-MM-DD HH:mm:ss')
    const end: string = moment(this.start).endOf('day').format('yyyy-MM-DD HH:mm:ss');
    let eventCards: IEventCard[] = [];

    this.schedule.days = [
      moment(this.start)
      .set({
        hour: 0,
        minute: 0,
        second: 0,
      }).format('yyyy-MM-DD')
    ];

    const dayEvents = this.events.filter(event =>
      moment(start).isSame(event.startAt, 'day') ||
      moment(end).isSame(event.endAt, 'day')
    );

    dayEvents.forEach(event => {
      let h =
        parseInt(moment(event.endAt).format('HH'),10) -
        parseInt(moment(event.startAt).format('HH'), 10);
      h = h > 24 ? 24 : h;

      eventCards.push({
        ...event,
        x: 1,
        y: parseInt(moment(event.startAt).format('HH'), 10) + 1,
        h: h,
        w: '700px',
      });
    });
    this.schedule.events = eventCards;
  }

  ngOnChanges()	{
    if(this.type === 'Week') {
      this.generateWeekSchedule();
    } else if (this.type === 'Day') {
      this.generateDaySchedule();
    }
  }

  createEvent(day: string, hour:string) {
    const date = moment(`${day} ${hour}`);

    const params = {
      name: '',
      start_day: moment(date).format('yyyy-MM-DD'),
      start_hour: moment(date).format('HH:mm'),
      end_hour: moment(date).add(1, 'hours').format('HH:mm'),
      end_day: moment(date).format('yyyy-MM-DD'),
      description: '',
      tz: momentTz.tz.guess(),
    }

    this.router.navigate(['/calendar/new'], {queryParams: { ...params }});
  }
}
