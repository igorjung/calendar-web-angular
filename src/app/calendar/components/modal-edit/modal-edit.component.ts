import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import * as momentTz from 'moment-timezone';

import { ICreateEvent } from '../../../shared/models/event.model';
import { defaultHours } from '../../../shared/utils/default-values';

import { CalendarService } from '../../calendar.service';

@Component({
  selector: 'app-modal-edit',
  templateUrl: './modal-edit.component.html',
})
export class ModalEditComponent implements OnInit {
  id: number = 0;
  hours: string[] = defaultHours;
  guests: string[] = [];
  isGuest: boolean = false;

  eventForm: FormGroup = this.formBuilder.group({});

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private calendarService: CalendarService,
  ) {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd && this.router.url.includes('/calendar/')) {
        const button = <HTMLElement>document.querySelector('button.modal');
        button.click();
      }
    });
  }

  open(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'new-event'});
  }

  close() {
    this.modalService.dismissAll();
    this.router.navigate(['calendar']);
  }

  isValidEmail(email: string): boolean {
    const user = JSON.parse(localStorage.getItem('user') || '');
    const validate = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return validate.test(String(email).toLowerCase()) && email !== user.email;
  }

  onAddGuests(guest:string) {
    if(guest && this.isValidEmail(guest)) {
      const list: string[] = [...this.guests];
      const isGuest = list.find((item) => item === guest);

      if(!isGuest) {
        list.push(guest);
        this.guests = list;
      }
    }
  }

  onDeleteGuest(guest:string) {
    if(guest) {
      const list: string[] = [...this.guests];
      const index: number = list.indexOf(guest);

      list.splice(index, 1);
      this.guests = list;
    }
  }

  onSubmit(): void {
    const value = this.eventForm.value;
    let start = moment(`${value.start_day} ${value.start_hour}`);
    let end = moment(`${value.start_day} ${value.end_hour}`);

    if(value.all_day) {
      start = moment(value.start_day).startOf('day');
      end = moment(value.end_day).endOf('day');
    }

    const newEvent: ICreateEvent  = {
      title: value.name,
      description: value.description,
      startAt: start.format('yyyy-MM-DD HH:mm:ss'),
      endAt: end.format('yyyy-MM-DD HH:mm:ss'),
      tz: momentTz.tz.guess(),
    }
    const guests = JSON.stringify(this.guests);

    this.calendarService.editEvent(newEvent, this.id, guests);
  }

  onDelete(): void {
    this.modalService.dismissAll();
    this.router.navigate(['calendar']);
    this.calendarService.deleteEvent(this.id);
  }

  ngOnInit() {
    this.id = parseFloat(this.route.snapshot.paramMap.get('id') || '');
    this.calendarService.newEvent.subscribe((event) => {
      const allDay: boolean =
        !moment(event.startAt).isSame(event.endAt, 'day') ||
        moment(event.startAt).format('HH:mm') === '00:00' &&
        moment(event.endAt).format('HH:mm') === '23:59';
      this.guests = event.guests;
      this.isGuest = event.isGuest;
      this.eventForm = this.formBuilder.group({
        name: [{value:event.title, disabled: this.isGuest}],
        start_day:[{
          value:moment(event.startAt).format('yyyy-MM-DD'),
          disabled: this.isGuest
        }],
        end_day: [{
          value:moment(event.endAt).format('yyyy-MM-DD'),
          disabled: this.isGuest
        }],
        start_hour: [{
          value:moment(event.startAt).format('HH:mm'),
          disabled: this.isGuest
        }],
        end_hour: [{
          value:moment(event.endAt).format('HH:mm'),
          disabled: this.isGuest
        }],
        all_day: [{value:allDay, disabled: this.isGuest}],
        description: [{value:event.description, disabled: this.isGuest}],
      });
    });

    this.calendarService.getEvent(this.id);
  }
}
