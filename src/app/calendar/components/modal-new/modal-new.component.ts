import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import * as momentTz from 'moment-timezone';

import { ICreateEvent } from '../../../shared/models/event.model';
import { defaultHours } from '../../../shared/utils/default-values';

import { CalendarService } from '../../calendar.service';

@Component({
  selector: 'app-modal-new',
  templateUrl: './modal-new.component.html',
})
export class ModalNewComponent {
  hours: string[] = defaultHours;
  guests: string[] = [];

  eventForm = this.formBuilder.group({
    name: '',
    start_day: moment(new Date()).format('yyyy-MM-DD'),
    end_day: moment(new Date()).format('yyyy-MM-DD'),
    start_hour: '09:00',
    end_hour: '10:00',
    all_day: false,
    description: '',
  });

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private calendarService: CalendarService,
  ) {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd && this.router.url.includes('/calendar/new')) {
        const button = <HTMLElement>document.querySelector('button.modal');
        button.click();
      }
    });

    this.route.queryParams.subscribe(params => {
      if(params.start_day) {
        this.eventForm =  this.formBuilder.group({
          ...params,
          all_day: false,
        });
      }
    });
  }

  open(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'new-event'});
  }

  close() {
    this.modalService.dismissAll();
    this.router.navigate(['calendar']);
  }

  isValidEmail(email: string): boolean {
    const user = JSON.parse(localStorage.getItem('user') || '');
    const validate = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return validate.test(String(email).toLowerCase()) && email !== user.email;
  }

  onAddGuests(guest:string) {
    if(guest && this.isValidEmail(guest)) {
      const list: string[] = [...this.guests];
      const isGuest = list.find((item) => item === guest);

      if(!isGuest) {
        list.push(guest);
        this.guests = list;
      }
    }
  }

  onDeleteGuest(guest:string) {
    if(guest) {
      const list: string[] = [...this.guests];
      const index: number = list.indexOf(guest);

      list.splice(index, 1);
      this.guests = list;
    }
  }

  onSubmit(): void {
    const value = this.eventForm.value;
    let start = moment(`${value.start_day} ${value.start_hour}`);
    let end = moment(`${value.start_day} ${value.end_hour}`);

    if(value.all_day) {
      start = moment(value.start_day).startOf('day');
      end = moment(value.end_day).endOf('day');
    }

    const newEvent: ICreateEvent  = {
      title: value.name,
      description: value.description,
      startAt: start.format('yyyy-MM-DD HH:mm:ss'),
      endAt: end.format('yyyy-MM-DD HH:mm:ss'),
      tz: momentTz.tz.guess(),
    }
    const guests = JSON.stringify(this.guests);

    this.calendarService.createEvent(newEvent, guests);
  }
}
