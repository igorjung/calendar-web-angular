import { Component, Input } from '@angular/core';
import * as moment from 'moment';

import { CalendarService } from '../../../calendar/calendar.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent {

  @Input() start: string = moment(new Date()).format('yyyy-MM-DD HH:mm:ss');
  @Input() type:string = 'Week';

  constructor(private calendarService:CalendarService) { }

  onSetToday() {
    this.start = moment(new Date()).format('yyyy-MM-DD HH:mm:ss');
    this.calendarService.setNewStart(this.start);
  }

  onChangeType(type: string) {
    this.type = type;
    this.calendarService.setNewType(this.type);
  }

  onChangeStart(value: string) {
    this.calendarService.setNewStart(this.start, value);
  }
}
