import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDate, NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

import { CalendarService } from '../../../calendar/calendar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent {

  @Input() model: NgbDateStruct;
  @Input() date: {year: number, month: number} = {
    year: parseInt(moment(new Date()).format('YYYY'), 10),
    month: parseInt(moment(new Date()).format('MM'), 10),
  };

  constructor(
    private router:Router,
    private calendarService:CalendarService,
    private calendar: NgbCalendar
  ){
    this.model = this.calendar.getToday();
  }

  createEvent(){
		this.router.navigate(['calendar/new']); // navigate to other page
	}

  onSetDate(date: NgbDate) {
    const newDate = moment(`${date.year}-${date.month}-${date.day} 00:00:00`)
    .format('yyyy-MM-DD HH:mm:ss');

    this.calendarService.setNewStart(newDate);
  }
}
